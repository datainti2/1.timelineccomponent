import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasewidgetComponent } from './basewidget.component';

describe('BasewidgetComponent', () => {
  let component: BasewidgetComponent;
  let fixture: ComponentFixture<BasewidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasewidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasewidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
